export interface CalculatorButtonProps {
  onClick: () => void;
  className?: string;
  children: any;
}

export default function CalculatorButton({
  onClick,
  className,
  children,
}: CalculatorButtonProps) {
  return (
    <div onClick={onClick} className={"button " + className}>
      <div>{children}</div>
    </div>
  );
}
