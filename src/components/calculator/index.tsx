/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-eval */
import CalculatorInput from "./CalculatorInput";
import CalculatorButton from "./button";
import React from "react";

export interface CalculatorProps {
  setVisibleCalc: (bool: boolean) => void;
  visibleCalc: boolean;
}

export default function Calculator({
  setVisibleCalc,
  visibleCalc,
}: CalculatorProps) {
  const [stacks, setStacks] = React.useState<string[]>([]);
  const [result, setResult] = React.useState<number | null | any>(null);

  const onClickExit = () => {
    setVisibleCalc(false);
  };

  const isArif = (val: string | null) => {
    return val === "*" || val === "+" || val === "-" || val === "/";
  };

  const onButtonClick = (val: string) => () => {
    let _stacks = [...stacks];
    let pop = _stacks.pop();
    let _result: any = null;
    if (isNaN(parseFloat(val))) {
      if (val === "Ac") {
        _stacks = [];
      } else if (val === "remove" && pop && pop.length > 1) {
        let pops = pop.split("");
        pops.pop();
        pop = pops.join("");
        _stacks.push(pop);
      } else if (pop && val === "=") {
        if (isArif(pop)) {
          _stacks.push(pop);
        } else {
          _stacks.push(pop);
          if (_stacks.length > 0) _result = null;
          const _stack2 = [..._stacks];
          _stacks = [eval(_stack2.join(""))];
        }
      } else if (pop && val === ".") {
        pop += ".";
        _stacks.push(pop);
      } else if (pop && isArif(val)) {
        if (isArif(pop)) {
          pop = val;
          _stacks.push(val);
        } else {
          _stacks.push(pop, val);
        }
        // setResult(null);
      }
    } else {
      if (!pop) {
        _stacks.push(val);
      } else if (isNaN(parseFloat(pop))) {
        _stacks.push(pop);
        _stacks.push(val);
      } else {
        // _stacks.push();
        let popValue = parseFloat(`${pop}${val}`).toString();
        if (popValue.length >= 10) {
          popValue = pop;
        }
        _stacks.push(popValue);
      }
      if (_stacks.length > 1) _result = eval(_stacks.join(""));
    }
    setResult(_result);
    setStacks(_stacks);

    // switch (val) {
    //   case "1":
    //   case "1":
    //   case "2":
    //   case "3":
    //   case "4":
    //   case "5":
    //   case "6":
    //   case "1":
    //   case "1":
    //   case "1": {
    //     break;
    //   }
    // }
  };

  return (
    <div
      className={`calculator ${!visibleCalc && "calc-closed"}`}
      //   style={{ display: visibleCalc ? "inherit" : "none" }}
    >
      <div className="calculator-body">
        <div
          style={{
            width: "540px",
            position: "relative",
            height: "900px",
          }}
        >
          <div className="calculator-content">
            <div className="input">
              <CalculatorInput
                arifmetic={
                  stacks.length > 0
                    ? stacks.map((e) => {
                        if (isArif(e)) {
                          return <span className="arifmetik">{e}</span>;
                        }

                        return e;
                      })
                    : [" "]
                }
                result={result ? `=${result}` : ""}
              />
            </div>
            <div className="buttons">
              <CalculatorButton
                onClick={onButtonClick("Ac")}
                className="remove-btn"
              >
                Ac
              </CalculatorButton>

              <CalculatorButton
                onClick={onButtonClick("remove")}
                className="remove-btn"
              >
                <img src="/icons/delete.png" />
              </CalculatorButton>

              <CalculatorButton
                onClick={onButtonClick("/")}
                className="arifmetik"
              >
                /
              </CalculatorButton>

              <CalculatorButton
                onClick={onButtonClick("*")}
                className="arifmetik"
              >
                *
              </CalculatorButton>

              <CalculatorButton onClick={onButtonClick("7")} className="number">
                7
              </CalculatorButton>

              <CalculatorButton onClick={onButtonClick("8")} className="number">
                8
              </CalculatorButton>
              <CalculatorButton onClick={onButtonClick("9")} className="number">
                9
              </CalculatorButton>

              <CalculatorButton
                onClick={onButtonClick("-")}
                className="arifmetik"
              >
                -
              </CalculatorButton>

              <CalculatorButton onClick={onButtonClick("4")} className="number">
                4
              </CalculatorButton>

              <CalculatorButton onClick={onButtonClick("5")} className="number">
                5
              </CalculatorButton>
              <CalculatorButton onClick={onButtonClick("6")} className="number">
                6
              </CalculatorButton>

              <div className="span-col-2 ac-1 ">
                <CalculatorButton
                  onClick={onButtonClick("+")}
                  className="arifmetik"
                >
                  +
                </CalculatorButton>
                <CalculatorButton
                  onClick={onButtonClick("=")}
                  className="arifmetik"
                >
                  =
                </CalculatorButton>
              </div>

              <CalculatorButton onClick={onButtonClick("1")} className="number">
                1
              </CalculatorButton>

              <CalculatorButton onClick={onButtonClick("2")} className="number">
                2
              </CalculatorButton>
              <CalculatorButton onClick={onButtonClick("3")} className="number">
                3
              </CalculatorButton>

              <CalculatorButton
                onClick={onButtonClick("0")}
                className="number number-zero mt-10px"
              >
                0
              </CalculatorButton>
              <CalculatorButton
                onClick={onButtonClick(".")}
                className="number mt-10px"
              >
                .
              </CalculatorButton>
            </div>
            <div className="box-shadow-1"></div>

            <div className="box-shadow-2"></div>
          </div>
          <div className="exit-btn " onClick={onClickExit}>
            <img src="/icons/exit.png" height={24} width={24} alt="exit" />
          </div>
        </div>
      </div>
    </div>
  );
}

export function CalculatorVisibleButton({
  setVisibleCalc,
  visibleCalc,
}: CalculatorProps) {
  if (visibleCalc) return null;
  return (
    <div
      onClick={() => {
        setVisibleCalc(true);
      }}
      className="calc-visible-button"
    >
      <img src="/icons/calculator.svg" style={{ width: 36, height: 36 }} />
    </div>
  );
}
