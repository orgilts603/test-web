import React from "react";

export interface CalculatorInputProps {
  arifmetic: React.ReactNode;
  result: string;
}

export default function CalculatorInput({
  arifmetic,
  result,
}: CalculatorInputProps) {
  return (
    <div className="input-content">
      <div className="text-1">{arifmetic}</div>
      <div className="result" style={{}}>
        {result}
      </div>
    </div>
  );
}
