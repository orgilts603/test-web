import React from "react";

export default function Header() {
  return (
    <div id="header">
      <div className="container header">
        <img src="/logo.png" style={{ width: 180 }} />
        <div className="menus">
          <div className="menu-item">CATALOGUE</div>
          <div className="menu-item">FASHION</div>
          <div className="menu-item">FAVOURITE</div>
          <div className="menu-item">CATALOGUE</div>
          <div className="menu-item menu-signup-item">SIGN UP</div>
        </div>
      </div>
    </div>
  );
}
