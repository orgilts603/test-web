import React from "react";

export default function HeroSection() {
  return (
    <section className="hero-section">
      <div className="container">
        <div className="sentence-container">
          <div className="hero-text">
            <div
              style={{
                width: "70%",
                height: "auto",
                transform: "rotate(-2deg)",
                transformOrigin: "0 0",
                background: "white",
                paddingLeft: 40,
              }}
            >
              LET’S
            </div>
            <div
              style={{
                paddingLeft: 40,
              }}
            >
              EXPLORE
            </div>
            <div
              style={{
                width: "80%",
                // height: "70%",
                transform: "rotate(-2deg)",
                transformOrigin: "0 0",
                background: "#EBD96B",
                paddingLeft: 40,
              }}
            >
              UNIQUE
            </div>
            <div style={{ paddingLeft: 40 }}>CLOTHES</div>
          </div>
          <div className="hero-desc">
            Live for Influential and Innovative fashion!
          </div>
          <div className="shop-now">SHOP NOW</div>
        </div>
        <div className="image-container">
          <img src="/hero_background.png" />{" "}
          <img src="/hero.png" className="hero-image" />
        </div>
      </div>
    </section>
  );
}
