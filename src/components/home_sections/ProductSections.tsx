export default function ProductSections() {
  return (
    <div className="product_sections">
      <div className="container">
        <h2>NEW ARRIVALS</h2>
        <div className="products">
          <ProductItem
            image="/models/1.png"
            subTitle="Explore Now!"
            title="Hoodies & Sweatshirt"
          />
          <ProductItem
            image="/models/2.png"
            subTitle="Explore Now!"
            title="Coats & Parkas"
          />
          <ProductItem
            image="/models/3.png"
            subTitle="Explore Now!"
            title="Tees & T-Shirt"
          />
          <ProductItem
            image="/models/1.png"
            subTitle="Explore Now!"
            title="Hoodies & Sweatshirt"
          />
          <ProductItem
            image="/models/2.png"
            subTitle="Explore Now!"
            title="Coats & Parkas"
          />
          <ProductItem
            image="/models/3.png"
            subTitle="Explore Now!"
            title="Tees & T-Shirt"
          />
        </div>
      </div>
    </div>
  );
}
function ProductItem({
  image,
  title,
  subTitle,
}: {
  image: string;
  title: string;
  subTitle: string;
}) {
  return (
    <div className="product-item">
      <img src={image} />
      <div className="title">{title}</div>
      <div className="sub-title">{subTitle}</div>
    </div>
  );
}
