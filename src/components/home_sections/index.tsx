import React from "react";
import Header from "./header";
import HeroSection from "./HeroSection";
import CustomerSection from "./CustomerSection";
import ProductSections from "./ProductSections";

export default function HomePage() {
  return (
    <React.Fragment>
      <Header />
      <HeroSection />
      <CustomerSection />
      <ProductSections />
    </React.Fragment>
  );
}
