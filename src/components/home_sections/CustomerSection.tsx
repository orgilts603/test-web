import React from "react";

export default function CustomerSection() {
  return (
    <section className="customer-section">
      <div className="container">
        <img src="/customers/1.png" />

        <img src="/customers/2.png" />

        <img src="/customers/3.png" />

        <img src="/customers/4.png" />

        <img src="/customers/5.png" />

        <img src="/customers/6.png" />
      </div>
    </section>
  );
}
