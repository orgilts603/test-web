/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Calculator, { CalculatorVisibleButton } from "./components/calculator";
import HomePage from "./components/home_sections";

function App() {
  const [visibleCalc, setVisibleCalc] = React.useState(false);

  React.useEffect(() => {
    let header = document.getElementById("header");
    if (!header) return;
    window.onscroll = function () {
      header && onScroll();
    };

    let sticky = header.offsetTop;

    function onScroll() {
      let header = document.getElementById("header");
      if (!header) return;
      if (window.pageYOffset > sticky) {
        header.classList.add("fixed");
      } else {
        header.classList.remove("fixed");
      }
    }
  }, [document.getElementById("header")]);

  return (
    <React.Fragment>
      <Calculator visibleCalc={visibleCalc} setVisibleCalc={setVisibleCalc} />
      <HomePage />
      <CalculatorVisibleButton
        visibleCalc={visibleCalc}
        setVisibleCalc={setVisibleCalc}
      />
    </React.Fragment>
  );
}

export default App;
