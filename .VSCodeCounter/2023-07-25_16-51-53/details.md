# Details

Date : 2023-07-25 16:51:53

Directory /Users/taivan/Desktop/workspace/test_web/web/src

Total : 20 files,  856 codes, 34 comments, 141 blanks, all 1031 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.css](/src/App.css) | CSS | 33 | 0 | 6 | 39 |
| [src/App.test.tsx](/src/App.test.tsx) | TypeScript JSX | 8 | 0 | 2 | 10 |
| [src/App.tsx](/src/App.tsx) | TypeScript JSX | 36 | 1 | 7 | 44 |
| [src/components/calculator/CalculatorInput.tsx](/src/components/calculator/CalculatorInput.tsx) | TypeScript JSX | 18 | 0 | 3 | 21 |
| [src/components/calculator/button.tsx](/src/components/calculator/button.tsx) | TypeScript JSX | 16 | 0 | 2 | 18 |
| [src/components/calculator/index.tsx](/src/components/calculator/index.tsx) | TypeScript JSX | 209 | 20 | 23 | 252 |
| [src/components/home_sections/CustomerSection.tsx](/src/components/home_sections/CustomerSection.tsx) | TypeScript JSX | 15 | 0 | 7 | 22 |
| [src/components/home_sections/HeroSection.tsx](/src/components/home_sections/HeroSection.tsx) | TypeScript JSX | 52 | 1 | 2 | 55 |
| [src/components/home_sections/ProductSections.tsx](/src/components/home_sections/ProductSections.tsx) | TypeScript JSX | 58 | 0 | 1 | 59 |
| [src/components/home_sections/header.tsx](/src/components/home_sections/header.tsx) | TypeScript JSX | 17 | 0 | 2 | 19 |
| [src/components/home_sections/index.tsx](/src/components/home_sections/index.tsx) | TypeScript JSX | 15 | 0 | 2 | 17 |
| [src/index.css](/src/index.css) | CSS | 10 | 0 | 3 | 13 |
| [src/index.tsx](/src/index.tsx) | TypeScript JSX | 15 | 3 | 2 | 20 |
| [src/logo.svg](/src/logo.svg) | XML | 1 | 0 | 0 | 1 |
| [src/react-app-env.d.ts](/src/react-app-env.d.ts) | TypeScript | 0 | 1 | 1 | 2 |
| [src/reportWebVitals.ts](/src/reportWebVitals.ts) | TypeScript | 13 | 0 | 3 | 16 |
| [src/setupTests.ts](/src/setupTests.ts) | TypeScript | 1 | 4 | 1 | 6 |
| [src/styles/sass/_calculator.scss](/src/styles/sass/_calculator.scss) | SCSS | 184 | 4 | 44 | 232 |
| [src/styles/sass/home_page.scss](/src/styles/sass/home_page.scss) | SCSS | 151 | 0 | 27 | 178 |
| [src/styles/sass/index.scss](/src/styles/sass/index.scss) | SCSS | 4 | 0 | 3 | 7 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)