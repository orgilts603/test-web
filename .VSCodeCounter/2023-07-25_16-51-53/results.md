# Summary

Date : 2023-07-25 16:51:53

Directory /Users/taivan/Desktop/workspace/test_web/web/src

Total : 20 files,  856 codes, 34 comments, 141 blanks, all 1031 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript JSX | 11 | 459 | 25 | 53 | 537 |
| SCSS | 3 | 339 | 4 | 74 | 417 |
| CSS | 2 | 43 | 0 | 9 | 52 |
| TypeScript | 3 | 14 | 5 | 5 | 24 |
| XML | 1 | 1 | 0 | 0 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 20 | 856 | 34 | 141 | 1,031 |
| . (Files) | 9 | 117 | 9 | 25 | 151 |
| components | 8 | 400 | 21 | 42 | 463 |
| components/calculator | 3 | 243 | 20 | 28 | 291 |
| components/home_sections | 5 | 157 | 1 | 14 | 172 |
| styles | 3 | 339 | 4 | 74 | 417 |
| styles/sass | 3 | 339 | 4 | 74 | 417 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)